package com.Laptop.Details.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.Laptop.Details.DTO.LaptopDTO;
import com.Laptop.Details.Utils.Factory;

public class LaptopDAO 
{
	//Method to Save Details
	
  public void saveLaptopDetails(LaptopDTO laptopDTO)
  {
	 SessionFactory sessionFactory = Factory.getInstance();
	 Session session = sessionFactory.openSession();
	 Transaction transaction = session.beginTransaction();
	 session.save(laptopDTO);
	 transaction.commit();
	  
  }
  
  //Method to get Laptop Details by id
  
  public LaptopDTO getLaptopDetailsById(String id)
  {
	  SessionFactory sessionFactory = Factory.getInstance();
	  Session session = sessionFactory.openSession();
	  return session.get(LaptopDTO.class, id);
  }
  
  //Method to Update Laptop price by id
  
  public void updateLaptopPriceByid(String id,Double price)
  {
	  LaptopDTO laptopDTO = getLaptopDetailsById(id);
	  if(laptopDTO!=null)
	  {
	  SessionFactory sessionFactory = Factory.getInstance();
	  Session session = sessionFactory.openSession();
	  Transaction transaction = session.beginTransaction();
	  laptopDTO.setPrice(price);
	  session.update(laptopDTO);
	  transaction.commit();
	  System.out.println("Update Successful!");
	  }
	  else
	  {
		  System.out.println("Invalid Id!!");
	  }
	 
  }
  
  //Method to Delete Laptop Details by id
  
  public void deleteLaptopDetailsById(String id)
  {
	  LaptopDTO laptopDTO = getLaptopDetailsById(id);
	  if(laptopDTO!=null)
	  {
	  SessionFactory sessionFactory = Factory.getInstance();
	  Session session = sessionFactory.openSession();
	  Transaction transaction = session.beginTransaction();
	  session.delete(laptopDTO);
	  transaction.commit();
	  System.out.println("Deletion Successful!");
	  }
	  else
	  {
		  System.out.println("Invalid Id!!");
	  }
	  
  }
  
  //*******HQL Methods********
  
  
  //Method to fetch data from database using HQL
  
  public List<LaptopDTO> getLaptopDetailsByHQL()
  {
	  SessionFactory sessionFactory = Factory.getInstance();
	  Session session = sessionFactory.openSession();
	  String hql="from LaptopDTO";
	  Query query = session.createQuery(hql);
	  query.setCacheable(true);
	  List <LaptopDTO> list = query.list();
	  return list;
  }
  
  //Method to fetch data from database using HQL by id
  
  public LaptopDTO getLaptopDetailsByIdUsingHQL(String id)
  {
	  SessionFactory sessionFactory = Factory.getInstance();
	  Session session = sessionFactory.openSession();
	  String hql="from LaptopDTO where id=: pId";
	  Query query = session.createQuery(hql);
	  query.setParameter("pId", id);
	  query.setCacheable(true);
      LaptopDTO uniqueResult = (LaptopDTO) query.uniqueResult();
	  return uniqueResult;
  }
  
  //Method to update price by id using HQL
   
  public void updatePriceByIdUsingHQL(String id,Double newPrice)
  {
	  SessionFactory sessionFactory = Factory.getInstance();
	  Session session = sessionFactory.openSession();
	  Transaction transaction = session.beginTransaction();
	  String hql="update LaptopDTO set price=: upPrice where id=:pId";
	  Query query = session.createQuery(hql);
	  query.setParameter("upPrice", newPrice);
	  query.setParameter("pId", id);
	  int executeUpdate = query.executeUpdate();
	  transaction.commit();
	  if(executeUpdate!=0)
	  {
		  System.out.println("Price Updated Successfully");
		  System.out.println();
    	  System.out.println(newPrice+" " + "has been set to the selected Model id.");
	  }
	  else
	  {
		  System.err.println("Update Failed!!");
		  System.err.println("Invalid id!!");
    	  System.out.println("Process Terminated!");
		  return;
	  }
	  
	  
  }
  
  //Method to Delete Record from Database by id
  
  public void deleteRecordByIdUsingHQL(String id)
  {
	  SessionFactory sessionFactory = Factory.getInstance();
	  Session session = sessionFactory.openSession();
	  Transaction transaction = session.beginTransaction();
	  String hql="delete LaptopDTO where id=:pId";
	  Query query = session.createQuery(hql);
	  query.setParameter("pId", id);
	  int executeUpdate = query.executeUpdate();
	  transaction.commit();
	  if(executeUpdate!=0)
	  {
		  System.out.println();
		  System.out.println("Your Record associated with id " +id+" has been deleted Successfully!");
	  }
	  else
	  {
		  System.err.println("Delete operation Failed!!");
		  System.out.println("Please check your id.");
		  return;
	  }
	  
  }
  
}
