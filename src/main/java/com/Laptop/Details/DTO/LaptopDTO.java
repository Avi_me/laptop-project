package com.Laptop.Details.DTO;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "laptop")
public class LaptopDTO implements Serializable
{
	@Id
	@GenericGenerator(name = "lap_auto",strategy = "com.Laptop.Details.Utils.CustomGenerator")
	@GeneratedValue(generator = "lap_auto")
	@Column(name = "ID")
	private String id;
	@Column(name = "Brand")
	private String brand;
	@Column(name = "Model")
	private String model;
	@Column(name = "Specifications")
	private String specifications;
	@Column(name = "Price")
	private Double price;
	
	public LaptopDTO() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getSpecifications() {
		return specifications;
	}

	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Override
	public String toString()
	{
		return " Laptop Details: "+"\n"+ " Id= " + id + "\n" +" Brand= " + brand + "\n" +" Model= " + model +"\n"+" Specifications= " +specifications+"\n"
				+" Price= " + price;
	}
	

}
