package com.Laptop.Details.Utils;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Factory 
{
	private static SessionFactory sess=null;
	
	private Factory()
	{
		
	}
	public static SessionFactory getInstance()
	{
		if(sess==null)
			sess=new Configuration().configure().buildSessionFactory();
		return sess;
	}
}
