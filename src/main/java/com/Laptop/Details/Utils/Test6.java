package com.Laptop.Details.Utils;

import java.util.Scanner;

import com.Laptop.Details.DAO.LaptopDAO;
import com.Laptop.Details.DTO.LaptopDTO;

public class Test6
{
	static int i=1;

	public static void main(String[] args) 
	{
		System.out.println("Enter id");
		@SuppressWarnings("resource")
		String id = new Scanner(System.in).next();
		
		LaptopDAO dao = new LaptopDAO();
		LaptopDTO dto = dao.getLaptopDetailsByIdUsingHQL(id);
		if(dto!=null)
		{
		System.out.println(dto);
		}
		else
		{
			System.err.println("Invalid Id");
			return;
		}
		if(i>1)
		{
			System.out.println("----------------");
			System.out.println("Process Completed.");
			return;
		}
		 cacheCheck();

	}
	public static void cacheCheck()
	{
		System.out.println();
		System.out.println("Cache is Enabled.To verify,Please enter the Same Id below.");
		System.out.println("----------------------------------------------------------");
		i++;
		main(null);
	}

}
