package com.Laptop.Details.Utils;

import java.util.List;

import com.Laptop.Details.DAO.LaptopDAO;
import com.Laptop.Details.DTO.LaptopDTO;

public class Test5 
{
	static int i=1;
	public static void main(String[] args)
	{
     LaptopDAO dao = new LaptopDAO();
     List<LaptopDTO> list = dao.getLaptopDetailsByHQL();
     System.out.println();
     System.out.println("**Records Found.**");
     System.out.println("--------------");
     for(LaptopDTO n:list)
     {
    	 System.out.println(n);
    	 System.out.println("-------------");
     }
     
     if(i>1)
     {
    	 System.out.println("--------------");
    	 System.out.println("Process Completed.");
     }
     else
     {
    	 System.out.println();
    	 System.err.println("Cache is Enabled.");
    	 System.err.println("Executing Same code for Verification.");
    	 i++;
    	 main(args);
     }
     
	}
}
