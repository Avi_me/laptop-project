package com.Laptop.Details.Utils;

import com.Laptop.Details.DAO.LaptopDAO;
import com.Laptop.Details.DTO.LaptopDTO;

public class Test1 {

	public static void main(String[] args)
	{
		 LaptopDTO lap = new LaptopDTO();
		  lap.setBrand("Apple");
		  lap.setModel("MacBook Pro");
		  lap.setSpecifications("2.6GHz,intel core i7,512GB HDD,16GB RAM");
		  lap.setPrice(198345.98D);
		  
		  LaptopDAO dao = new LaptopDAO();
		  dao.saveLaptopDetails(lap);
		  System.out.println("Details Saved Successfully!");

	}

}
