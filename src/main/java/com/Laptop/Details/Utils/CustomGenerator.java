package com.Laptop.Details.Utils;

import java.io.Serializable;
import java.util.Random;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

public class CustomGenerator implements IdentifierGenerator {

	public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException 
	{
		String s="IND";
		Random random = new Random();
		int n=random.nextInt((9999-100)+1);
		String res=s+n;
		return res;
	}

}